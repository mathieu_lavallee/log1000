#include "data.h"

int get_data(int index) {
    
  switch(index) {
    case 0: return 0; break;
    case 1: return 100; break;
    case 2: return 400; break;
    case 3: return 900; break;
    default: return -1; break;
  }
}