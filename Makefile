# Variables
EXEC=musaktron
CC=gcc

# Construit tout le logiciel
all: $(EXEC)

# Construction de l'executable
$(EXEC): main.o data.o
	$(CC) -o $@ $^

main.o: main.c data.h
	$(CC) -o $@ -c $<

data.o: data.c data.h
	$(CC) -o $@ -c $<

# Pour enlever les fichiers intermediaires
clean:
	rm -rf *.o